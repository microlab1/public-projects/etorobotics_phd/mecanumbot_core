# Mecanumbot core 

Core package to set up the onboard Raspberry Pi for a robot with mecanum wheels.

Tested with: <br> 
**Ubuntu 20.04** <br>
**ROS 1 - Noetic** <br>

This project took the Turtlebot3 robot as inspiration and base. The original ducumentation of the Turtlebot3 robots can be
seen at [Robotis eManual](https://emanual.robotis.com/docs/en/platform/turtlebot3/overview/#overview)

## Dependencies

Ros Packages: <br>
**turtlebot3** <br>
**turtlebot3_msgs** <br>

Installation of dependencies:

    [PI-Terminal] $ sudo apt install ros-noetic-turtlebot3-msgs
    [PI-Terminal] $ sudo apt install ros-noetic-turtlebot3


## Installation

To install the Raspberry PI 3 B+ with the proper ROS Noetic distro please 
follow the [Robotis eManul - SBC Setup](https://emanual.robotis.com/docs/en/platform/turtlebot3/sbc_setup/#sbc-setup) page.

To make it easier to follow the description 
- **[PC-Terminal]** is used to refer to the Master PC running the `roscore`.
- **[PI-Terminal]** is used to refer to the onboard Raspberry Pi on the robot.

### 1. Clone the repository
```
[PI-Terminal] $ cd ~/catkin_ws/src
[PI-Terminal] $ git clone https://gitlab.com/microlab1/private-projects/etorobotics/mecanumbot_core.git
[PI-Terminal] $ cd ~/catkin_ws
[PI-Terminal] $ catkin_make
```

### 2. Setup the OpenCR board firmware

To setup the OpenCR board follow [Robotis eManul - OpenCR Setup](https://emanual.robotis.com/docs/en/platform/turtlebot3/opencr_setup/#opencr-setup).
The required firmware is in the **mecanum_v1.0** folder. Use the Arduino IDE to flash the `mecanum_v1.0.ino` to the OpenCR board.

Copy the **firmware** folder into the Arduino IDE **sketch** folder <br> 
OR <br>
Set the Arduino IDE sketch folder preference to this folder under the 
**File/Preferences** option.

Upload the firmware to the OpenCR board. <br>

Important steps:
- Install OpenCR library into the Arduino IDE.
- Set the board type.
- Select the port.
- Set the OpenCR board into firmware update mode manually. (with Push SW2 and Reset button)
- Motors baudrate set to 115200. (Factory reset is recommended)

For more information about the installation and firmware update see the 
[Robotis eManual](https://emanual.robotis.com/docs/en/parts/controller/opencr10/). <br>

## Getting started

Mecanumbot IP and Master PC IP should be checked before every bringup. <br>
The following .bashrc scripts can help, to set the proper IP addresses connected to known wifi networks.

### Automatic IP setting for Raspberry .bashrc

    [PI-Terminal] $ nano ~/.bashrc
    [PI-Terminal] $ source ~/.bashrc

`.bashrc` file:

    export ROS_HOSTNAME=$(hostname -I | sed 's/ *$//g')

    if [[ "$ROS_HOSTNAME" = "10.0.0.12" ]]; then
        export ROS_MASTER_URI=http://10.0.0.113:11311
    elif [[ "$ROS_HOSTNAME" = "192.168.100.111" ]]; then
        export ROS_MASTER_URI=http://192.168.100.156:11311
    fi
    
    echo ROS_MASTER_URI=$ROS_MASTER_URI
    echo ROS_HOSTNAME=$ROS_HOSTNAME

### Automatic IP setting for Master PC .bashrc

    [PC-Terminal] $ nano ~/.bashrc
    [PC-Terminal] $ source ~/.bashrc

`.bashrc` file:

    export ROS_MASTER_URI=http://$(hostname -I | sed 's/ *$//g'):11311
    export ROS_IP=$(hostname -I | sed 's/ *$//g')

    echo ROS_MASTER_URI=$ROS_MASTER_URI
    echo ROS_IP=$ROS_IP


Please note that the automatic IP configs works within the MOGI departure according to to the following IPs.

Fix mecanum Raspberry IPs on different wifi networks at MOGI:<br>
IP: **10.0.0.12** (D411) <br>
IP: **192.168.1.103** (D422) <br>
IP: **192.168.100.111** (D510) <br>

### Bringup

Use ssh to reach the [PI-Terminal] on the Raspberry Pi 

Use the proper IP to ssh into the mounted Raspberry PI and make a **PI - Terminal**:

      $ ssh ubuntu@10.0.0.12

And Log in. <br>
Login: **ubuntu** <br>
password: **turtlebot** <br>

### Start the robot

    [PC-Terminal]: $ roscore
    [PI-Terminal]: $ roslaunch mecanumbot_core mecanumbot_robot.launch

To control the robot use the `/cmd_vel` topic with a `geometry_msgs/Twist` message.

With `mecanum_teleop` package installed use the `mecanum_teleop_key` node.

    [PC-Terminal]: $ rosrun mecanum_teleop mecanum_teleop_key

## Topic functionalities

| Topic                 | Functionality                                        |
|-----------------------|------------------------------------------------------|
| `/cmd_vel`            | Mecanumbot drive control                             |
| `/cmd_led`            | RGB LED stripe control                               |
| `/cmd_grabber`        | Grabber control                                      |
| `/cmd_neck`           | 2 DoF neck control                                   |
| `/rob_grabber`        | Grabber state feedback from robot                    |
| `/rob_imu`            | IMU sensor values from robot                         |
| `/rob_led`            | RGB LED colors feedback from robot                   |
| `/rob_mag`            | Magneto sensor values from robot                     |
| `/rob_neck`           | 2 DoF neck feedback from robot                       |
| `/rob_voltage`        | Voltage feedback from robot                          |
| `/scan`               | 360 Lidar sensor data from robot (Turtlebot3 legacy) |
| `/sensor_state`       | Sensor state feedback (Turtlebot3 legacy)            |
| `/version_info`       | Version information (Turtlebot3 legacy)              |
| `/battery_state`      | Battery state feedback                               |
| `/rpms`               | Lidar rpms feedback (Turtlebot3 legacy)              |
| `/diagnostics`        | (Turtlebot3 legacy)                                  |
| `/firmware_version`   | Firmware version (Turtlebot3 legacy)                 |


There is no service implemented on the robot. Only the `get_loggers` and `set_logger_level` are available for legacy reasons.

## Authors and acknowledgment
Balázs Nagy (email: nagybalazs@mogi.bme.hu)

## License
The project is open source under the lincense of BSD.

## ToDo

- [ ] Firmware does not contain the specific robot parameters
- [ ] Picture to show the robot
 