#ifndef DRIVER_H_
#define DRIVER_H_

#include <DynamixelSDK.h>


// Control table address (Dynamixel AX-12A)
#define ADDR_AX_TORQUE_ENABLE            24
#define ADDR_AX_GOAL_POSITION            30
#define ADDR_AX_PRESENT_POSITION         36
#define ADDR_AX_PRESENT_SPEED            38

// Data Byte Length
#define LEN_AX_TORQUE_ENABLE             1
#define LEN_AX_GOAL_POSITION             2
#define LEN_AX_PRESENT_POSITION          2
#define LEN_AX_PRESENT_SPEED             2

#define PROTOCOL_VERSION1               1.0     // Dynamixel protocol version 2.0

#define DXL_LEFT_GRABBER_ID                8        // ID of left rear motor
#define DXL_RIGHT_GRABBER_ID               18       // ID of right rear motor

#define BAUDRATE                        115200 // baud rate of Dynamixel
#define DEVICENAME                      ""      // no need setting on OpenCR

#define TORQUE_ENABLE                   1       // Value for enabling the torque
#define TORQUE_DISABLE                  0       // Value for disabling the torque

class GrabberMotorDriver
{
 public:
  GrabberMotorDriver();
  ~GrabberMotorDriver();
  bool init(void);
  void closeDynamixel(void);
  bool setTorque(uint8_t id, bool onoff);
  //bool setProfileAcceleration(uint8_t id, uint32_t value);
  //bool setProfileVelocity(uint8_t id, uint32_t value);
  bool controlMotor(int64_t left_grabber_value, int64_t right_grabber_value);

 private:
  uint32_t baudrate_;
  float  protocol_version_;
  uint8_t left_grabber_id_, right_grabber_id_;

  dynamixel::PortHandler *portHandler_;
  dynamixel::PacketHandler *packetHandler_;

  dynamixel::GroupSyncWrite *groupSyncWritePosition_;
};

#endif // DRIVER_H_
