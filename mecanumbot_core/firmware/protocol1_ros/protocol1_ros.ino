/*
 * Protocol1 ROS test = AX motor test
 * 
 * callback based 
 * 
 * Baudrate 115200 on every motor
 * [AX1] - [AX2] - MX1 - MX2 ------ OpenCR (serial connection) - Tested = Working!
 * 
 * roslaunch mecanum mecanum_port.launch
 * rostopic pub -1 /cmd_grabber geometry_msgs/Vector3 -- '1000.0' '1000.0' '0.0'
 * 
 */

#include "config.h"

void setup() {
  DEBUG_SERIAL.begin(57600);

  nh.initNode();
  nh.getHardware()->setBaud(115200);

  // Subscribers
  nh.subscribe(grabber_cmd);

  // Setting for Dynamixel motors
  grabber_driver.init();

  pinMode(13, OUTPUT);
  
}

void loop() {
  nh.spinOnce();
}

/*******************************************************************************
* Callback function for cmd_grabber
*******************************************************************************/
void cmdGrabberCallback(const geometry_msgs::Vector3& cmd_grabber){
  bool dxl_comm_result = false;
  dxl_comm_result = grabber_driver.controlMotor((int64_t)cmd_grabber.x, (int64_t)cmd_grabber.y);
  if (dxl_comm_result == false)
    return;
}
