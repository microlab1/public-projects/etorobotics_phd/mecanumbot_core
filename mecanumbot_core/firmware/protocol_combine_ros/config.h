#ifndef CONFIG_H_
#define CONFIG_H_

#define NOETIC_SUPPORT

#include <ros.h>
#include <ros/time.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Empty.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Vector3.h>

#include <math.h>

#include "driver.h"

// #define DEBUG                            
#define DEBUG_SERIAL                     SerialBT2
#define FIRMWARE_VER                     "1.1"

ros::NodeHandle nh;

NeckMotorDriver neck_driver;
GrabberMotorDriver grabber_driver;

void cmdNeckCallback(const geometry_msgs::Vector3& cmd_neck);
void cmdGrabberCallback(const geometry_msgs::Vector3& cmd_grabber);

ros::Subscriber<geometry_msgs::Vector3> neck_cmd("cmd_neck", &cmdNeckCallback);
ros::Subscriber<geometry_msgs::Vector3> grabber_cmd("cmd_grabber", &cmdGrabberCallback);

#endif // CONFIG_H_
