/*
 * Protocol combined ROS test = MX + AX motor test
 * 
 * callback based
 * 
 * Baudrate 115200 on every motor
 * AX1 - AX2 - [MX1] - [MX2] ------ OpenCR (serial connection) - Tested = Working!
 * 
 *  * roslaunch mecanum mecanum_port.launch
 * rostopic pub -1 /cmd_grabber geometry_msgs/Vector3 -- '1000.0' '1000.0' '0.0'
 * rostopic pub -1 /cmd_neck geometry_msgs/Vector3 -- '2000.0' '2000.0' '0.0'
 * 
 */

#include "config.h"

void setup() {
  DEBUG_SERIAL.begin(57600);

  nh.initNode();
  nh.getHardware()->setBaud(115200);

  // Subscribers
  nh.subscribe(neck_cmd);
  nh.subscribe(grabber_cmd);
  
  // Setting for Dynamixel motors
  neck_driver.init();
  grabber_driver.init();

  pinMode(13, OUTPUT);
  
}

void loop() {
  nh.spinOnce();
  waitForSerialLink(nh.connected());
}

/*******************************************************************************
* Callback function for cmd_neck
*******************************************************************************/
void cmdNeckCallback(const geometry_msgs::Vector3& cmd_neck){
  bool dxl_comm_result = false;
  
  dxl_comm_result = neck_driver.controlMotor((int64_t)cmd_neck.x, (int64_t)cmd_neck.y);
  if (dxl_comm_result == false)
    return;
}

/*******************************************************************************
* Callback function for cmd_grabber
*******************************************************************************/
void cmdGrabberCallback(const geometry_msgs::Vector3& cmd_grabber){
  bool dxl_comm_result = false;
  dxl_comm_result = grabber_driver.controlMotor((int64_t)cmd_grabber.x, (int64_t)cmd_grabber.y);
  if (dxl_comm_result == false)
    return;
}

/*******************************************************************************
* Wait for Serial Link
*******************************************************************************/
void waitForSerialLink(bool isConnected)
{
  static bool wait_flag = false;
  
  if (isConnected)
  {
    if (wait_flag == false)
    {      
      delay(10);

      wait_flag = true;
    }
  }
  else
  {
    wait_flag = false;
  }
}
