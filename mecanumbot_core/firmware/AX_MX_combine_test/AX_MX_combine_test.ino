/*
 * Upload the code
 * Open a Serial monitor
 * Send a character in
 * 
 * Baudrate is 115200 in every motor
 * AX1 - AX2 - XM1 - XM2 ----------OpenCR (serial connection test)
 */

#include <DynamixelSDK.h>

// Control table address for Dynamixel AX-12
#define ADDR_MX_TORQUE_ENABLE           24                  // Control table address is different in Dynamixel model
#define ADDR_MX_GOAL_POSITION           30
#define ADDR_MX_PRESENT_POSITION        36

// Control table address for Dynamixel XM430
#define ADDR_XM_TORQUE_ENABLE          64
#define ADDR_XM_GOAL_POSITION          116
#define ADDR_XM_PRESENT_POSITION       132

// XMtocol version
#define PROTOCOL_VERSION1               1.0                 // See which Protocol version is used in the Dynamixel
#define PROTOCOL_VERSION2               2.0

// Default setting
#define AX1_ID                          8                   // Dynamixel#1 ID: 1
#define AX2_ID                          18
#define XM1_ID                          13                   // Dynamixel#2 ID: 2
#define XM2_ID                          14
#define BAUDRATE                        115200
#define DEVICENAME                      "OpenCR_DXL_Port"   // This definition only has a symbolic meaning and does not affect to any functionality

#define TORQUE_ENABLE                   1                   // Value for enabling the torque
#define TORQUE_DISABLE                  0                   // Value for disabling the torque
#define AX_MINIMUM_POSITION_VALUE       0                 // Dynamixel will rotate between this value
#define AX_MAXIMUM_POSITION_VALUE       1023                // and this value (note that the Dynamixel would not move when the position value is out of movable range. Check e-manual about the range of the Dynamixel you use.)
#define XM_MINIMUM_POSITION_VALUE       0
#define XM_MAXIMUM_POSITION_VALUE       4095
#define AX_MOVING_STATUS_THRESHOLD      10                  // Dynamixel MX moving status threshold
#define XM_MOVING_STATUS_THRESHOLD      40                  // Dynamixel XM moving status threshold

#define ESC_ASCII_VALUE                 0x1b

#define CMD_SERIAL                      Serial

int getch()
{
  while(1)
  {
    if( CMD_SERIAL.available() > 0 )
    {
      break;
    }
  }

  return CMD_SERIAL.read();
}

int kbhit(void)
{
  return CMD_SERIAL.available();
}

void setup()
{
  Serial.begin(115200);
  while(!Serial);

  Serial.println("Start..");

  // Initialize PortHandler instance
  // Set the port path
  // Get methods and members of PortHandlerLinux or PortHandlerWindows
  dynamixel::PortHandler *portHandler = dynamixel::PortHandler::getPortHandler(DEVICENAME);

  // Initialize PacketHandler instance
  // Set the XMtocol version
  // Get methods and members of XMtocol1PacketHandler or XMtocol2PacketHandler
  dynamixel::PacketHandler *packetHandler1 = dynamixel::PacketHandler::getPacketHandler(PROTOCOL_VERSION1);
  dynamixel::PacketHandler *packetHandler2 = dynamixel::PacketHandler::getPacketHandler(PROTOCOL_VERSION2);

  int index = 0;
  int dxl_comm_result = COMM_TX_FAIL;       // Communication result
  int AX_goal_position[2] = {AX_MINIMUM_POSITION_VALUE, AX_MAXIMUM_POSITION_VALUE};     // Goal position of Dynamixel MX
  int XM_goal_position[2] = {XM_MINIMUM_POSITION_VALUE, XM_MAXIMUM_POSITION_VALUE};     // Goal position of Dynamixel XM

  uint8_t dxl_error = 0;                    // Dynamixel error
  uint16_t AX_present_position = 0;       // Present position of Dynamixel MX
  int32_t XM_present_position = 0;        // Present position of Dynamixel XM

  // Open port
  if (portHandler->openPort())
  {
    Serial.print("Succeeded to open the port!\n");
  }
  else
  {
    Serial.print("Failed to open the port!\n");
    return;
  }

  // Set port baudrate
  if (portHandler->setBaudRate(BAUDRATE))
  {
    Serial.print("Succeeded to change the baudrate!\n");
  }
  else
  {
    Serial.print("Failed to change the baudrate!\n");
    return;
  }

  // Enable Dynamixel#1 torque
  dxl_comm_result = packetHandler1->write1ByteTxRx(portHandler, AX1_ID, ADDR_MX_TORQUE_ENABLE, TORQUE_ENABLE, &dxl_error);
  dxl_comm_result = packetHandler1->write1ByteTxRx(portHandler, AX2_ID, ADDR_MX_TORQUE_ENABLE, TORQUE_ENABLE, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS)
  {
    Serial.print(packetHandler1->getTxRxResult(dxl_comm_result));
  }
  else if (dxl_error != 0)
  {
    Serial.print(packetHandler1->getRxPacketError(dxl_error));
  }
  else
  {
    Serial.print("Dynamixel#1 has been successfully connected \n");
  }
  // Enable Dynamixel#2 torque
  dxl_comm_result = packetHandler2->write1ByteTxRx(portHandler, XM1_ID, ADDR_XM_TORQUE_ENABLE, TORQUE_ENABLE, &dxl_error);
  dxl_comm_result = packetHandler2->write1ByteTxRx(portHandler, XM2_ID, ADDR_XM_TORQUE_ENABLE, TORQUE_ENABLE, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS)
  {
    Serial.print(packetHandler2->getTxRxResult(dxl_comm_result));
  }
  else if (dxl_error != 0)
  {
    Serial.print(packetHandler2->getRxPacketError(dxl_error));
  }
  else
  {
    Serial.print("Dynamixel#2 has been successfully connected \n");
  }

  while(1)
  {
    Serial.print("Press any key to continue! (or press q to quit!)\n");
    if (getch() == 'q')
      break;

    // Write Dynamixel#1 goal position
    dxl_comm_result = packetHandler1->write2ByteTxRx(portHandler, AX1_ID, ADDR_MX_GOAL_POSITION, AX_goal_position[index], &dxl_error);
    dxl_comm_result = packetHandler1->write2ByteTxRx(portHandler, AX2_ID, ADDR_MX_GOAL_POSITION, AX_goal_position[index], &dxl_error);
    if (dxl_comm_result != COMM_SUCCESS)
    {
      Serial.print(packetHandler1->getTxRxResult(dxl_comm_result));
    }
    else if (dxl_error != 0)
    {
      Serial.print(packetHandler1->getRxPacketError(dxl_error));
    }

    // Write Dynamixel#2 goal position
    dxl_comm_result = packetHandler2->write4ByteTxRx(portHandler, XM1_ID, ADDR_XM_GOAL_POSITION, XM_goal_position[index], &dxl_error);
    dxl_comm_result = packetHandler2->write4ByteTxRx(portHandler, XM2_ID, ADDR_XM_GOAL_POSITION, XM_goal_position[index], &dxl_error);
    if (dxl_comm_result != COMM_SUCCESS)
    {
      Serial.print(packetHandler2->getTxRxResult(dxl_comm_result));
    }
    else if (dxl_error != 0)
    {
      Serial.print(packetHandler2->getRxPacketError(dxl_error));
    }

    do
    {
      // Read Dynamixel#1 present position
      dxl_comm_result = packetHandler1->read2ByteTxRx(portHandler, AX1_ID, ADDR_MX_PRESENT_POSITION, &AX_present_position, &dxl_error);
      if (dxl_comm_result != COMM_SUCCESS)
      {
        Serial.print(packetHandler1->getTxRxResult(dxl_comm_result));
      }
      else if (dxl_error != 0)
      {
        Serial.print(packetHandler1->getRxPacketError(dxl_error));
      }

      // Read Dynamixel#2 present position
      dxl_comm_result = packetHandler2->read4ByteTxRx(portHandler, XM1_ID, ADDR_XM_PRESENT_POSITION, (uint32_t*)&XM_present_position, &dxl_error);
      if (dxl_comm_result != COMM_SUCCESS)
      {
        Serial.print(packetHandler2->getTxRxResult(dxl_comm_result));
      }
      else if (dxl_error != 0)
      {
        Serial.print(packetHandler2->getRxPacketError(dxl_error));
      }

      Serial.print("[ID:"); Serial.print(AX1_ID);
      Serial.print("] GoalPos:"); Serial.print(AX_goal_position[index]);
      Serial.print("  PresPos:"); Serial.print(AX_present_position);
      Serial.print("  [ID:"); Serial.print(XM1_ID);
      Serial.print("] GoalPos:"); Serial.print(XM_goal_position[index]);
      Serial.print("  PresPos:"); Serial.print(XM_present_position);

    }while((abs(AX_goal_position[index] - AX_present_position) > AX_MOVING_STATUS_THRESHOLD) || (abs(XM_goal_position[index] - XM_present_position) > XM_MOVING_STATUS_THRESHOLD));

    // Change goal position
    if (index == 0)
    {
      index = 1;
    }
    else
    {
      index = 0;
    }
  }

  // Disable Dynamixel#1 Torque
  dxl_comm_result = packetHandler1->write1ByteTxRx(portHandler, AX1_ID, ADDR_MX_TORQUE_ENABLE, TORQUE_DISABLE, &dxl_error);
  dxl_comm_result = packetHandler1->write1ByteTxRx(portHandler, AX2_ID, ADDR_MX_TORQUE_ENABLE, TORQUE_DISABLE, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS)
  {
    Serial.print(packetHandler1->getTxRxResult(dxl_comm_result));
  }
  else if (dxl_error != 0)
  {
    Serial.print(packetHandler1->getRxPacketError(dxl_error));
  }

  // Disable Dynamixel#2 Torque
  dxl_comm_result = packetHandler2->write1ByteTxRx(portHandler, XM1_ID, ADDR_XM_TORQUE_ENABLE, TORQUE_DISABLE, &dxl_error);
  dxl_comm_result = packetHandler2->write1ByteTxRx(portHandler, XM2_ID, ADDR_XM_TORQUE_ENABLE, TORQUE_DISABLE, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS)
  {
    Serial.print(packetHandler2->getTxRxResult(dxl_comm_result));
  }
  else if (dxl_error != 0)
  {
    Serial.print(packetHandler2->getRxPacketError(dxl_error));
  }

  // Close port
  portHandler->closePort();
}

void loop()
{
}
